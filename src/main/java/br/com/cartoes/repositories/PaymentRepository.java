package br.com.cartoes.repositories;

import br.com.cartoes.models.Payment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends CrudRepository<Payment, Integer> {

    Iterable<Payment> findByCreditCardId(int id);

}
