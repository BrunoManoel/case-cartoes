package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.CreditCardDTO;
import br.com.cartoes.DTOs.CreditCardUptadedDTO;
import br.com.cartoes.models.CreditCard;
import br.com.cartoes.services.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/creditcard")
public class CreditCardController {

    @Autowired
    CreditCardService creditCardService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public CreditCardDTO createCreditCard(@RequestBody CreditCardDTO creditCardDTO) {
        return creditCardService.createCreditCard(creditCardDTO);
    }

    @PatchMapping("/{number}")
    public CreditCardDTO updateCreditCard(@RequestBody CreditCardUptadedDTO creditCardDTO, @PathVariable(name = "number") String number) {
        return creditCardService.updateCreditCard(creditCardDTO, number);
    }

    @GetMapping("/{number}")
    public CreditCardDTO findByNumber(@PathVariable(name = "number") String number) {
        return creditCardService.findByNumber(number);
    }

}
