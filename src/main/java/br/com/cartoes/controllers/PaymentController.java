package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.PaymentDTO;
import br.com.cartoes.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("payments")
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @PostMapping
    public PaymentDTO createPayment(@RequestBody PaymentDTO paymentDTO) {
        return paymentService.createPaymente(paymentDTO);
    }

    @GetMapping("/{id}")
    public List<PaymentDTO> listPaymentsById(@PathVariable("id") int id) {
        return paymentService.listPaymentsByCreditCardId(id);
    }
}