package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.ClientDTO;
import br.com.cartoes.models.Client;
import br.com.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    ClienteService clienteService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public ClientDTO registerClient(@RequestBody ClientDTO clientDTO) {
        return clienteService.registerClient(clientDTO);
    }

    @GetMapping("/{id}")
    public ClientDTO findClientById(@PathVariable(name = "id") int id) {
        return clienteService.findClientBydId(id);
    }
}
