package br.com.cartoes.services;

import br.com.cartoes.DTOs.ClientDTO;
import br.com.cartoes.models.Client;
import br.com.cartoes.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    ClientRepository clientRepository;

    public ClientDTO registerClient(ClientDTO clientDTO) {

        Client client = new Client();
        client.setName(clientDTO.getName());
        clientRepository.save(client);

        return clientDTO;
    }

    public ClientDTO findClientBydId(int id) {
        Optional<Client> clientOptional = clientRepository.findById(id);

        if (clientOptional.isPresent()) {
            ClientDTO clientDTO = new ClientDTO();
            clientDTO.setId(clientOptional.get().getId());
            clientDTO.setName(clientOptional.get().getName());
            return clientDTO;
        } else {
            throw new RuntimeException("Client ID not found");
        }
    }
}